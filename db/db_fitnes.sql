-- phpMyAdmin SQL Dump
-- version 4.8.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: Aug 14, 2023 at 07:57 PM
-- Server version: 10.1.37-MariaDB
-- PHP Version: 5.6.40

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_fitnes`
--

-- --------------------------------------------------------

--
-- Table structure for table `berita`
--

CREATE TABLE `berita` (
  `id` int(11) NOT NULL,
  `judul_berita` text NOT NULL,
  `tipe_berita` varchar(20) NOT NULL,
  `status_berita` varchar(2) NOT NULL,
  `isi_berita` text NOT NULL,
  `created_date` date NOT NULL,
  `foto` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `berita`
--

INSERT INTO `berita` (`id`, `judul_berita`, `tipe_berita`, `status_berita`, `isi_berita`, `created_date`, `foto`) VALUES
(2, 'Berita 1', 'TP', 'A', 'Lorem Ipsum is simply dummy text of the printing and typesetting industry. Lorem Ipsum has been the industry\'s standard dummy text ever since the 1500s, when an unknown printer took a galley of type and scrambled it to make a type specimen book. It has survived not only five centuries, but also the leap into electronic typesetting, remaining essentially unchanged. It was popularised in the 1960s with the release of Letraset sheets containing Lorem Ipsum passages, and more recently with desktop publishing software like Aldus PageMaker including versions of Lorem Ipsum.', '2023-08-14', ''),
(3, 'Tips Gym Bagi Pemula', 'Tips Gym Bagi Pemula', 'A', 'Beberapa Tips Gym untuk Pemula\r\nSetiap orang memiliki alasan berbeda untuk bergabung dengan gym. Bagi yang belum pernah berolahraga di gym sebelumnya, mungkin rasa khawatir muncul ketika akan menggunakan peralatan yang ada. Hal ini wajar, tetapi sebaiknya tidak menyurutkan semangat kamu untuk disiplin berolahraga, ya.\r\n\r\nBila kamu masih pemula, cobalah untuk fokus pada teknik tetapi jangan terburu-buru. Beristirahatlah selama 60-90 detik di antara setiap set, tetapi teruslah lakukan gerakan ringan seperti jalan santai agar otot tetap hangat dan detak jantung meningkat. Idealnya, lakukan latihan dalam urutan yang tercantum.\r\n\r\nAda banyak cara pelatihan dan semuanya bisa bermanfaat tergantung pada tujuan kamu. Bagaimana cara kamu memilih untuk berlatih, ada beberapa tips gym untuk pemula yang dapat dilakukan untuk membantu mendapatkan hasil maksimal dari latihan, yaitu:\r\n\r\n1. Tetapkan Tujuan dengan Jelas\r\n\r\nLangkah pertama yang perlu dilakukan sebagai tips gym untuk pemula adalah meluangkan waktu untuk menetapkan tujuan dengan jelas. Mengapa kamu ingin nge-gym?\r\n\r\nBagaimana kamu akan memasukkan olahraga di gym ke dalam gaya hidup kamu? Setelah mengetahui jawabannya, lakukan saja semua yang bisa dilakukan.\r\n\r\nBaca juga: Ketahui Tren Fitnes di Tahun 2021 Agar Tetap Bugar\r\n\r\n2. Coba Mulai dengan 30 Menit sebanyak 3 Kali dalam Seminggu\r\n\r\nJika kamu baru memulai pergi ke gym, jangan terlalu memaksakan diri. Cobalah mulai dengan tiga hari seminggu, selama 30 menit saja. Pastikan bahwa kamu melatih diri dengan aman dan efektif. Kemudian, setelah minggu keempat, coba tambahkan 30 menit lagi setiap minggu.\r\n\r\n3. Perhatikan Apa yang Kamu Makan\r\n\r\nTerutama jika tujuan kamu adalah menurunkan berat badan, ingatlah prinsip dasarnya adalah lebih banyak kalori yang digunakan ketimbang yang masuk. Jika kamu berlatih untuk mendapatkan otot, jenis makanan yang kamu makan juga sangat penting. Tambahkan makanan tinggi protein, dan kurangi makanan tinggi lemak.\r\n\r\n4. Lakukan Pemanasan dengan Benar\r\n\r\nDalam hal menghangatkan tubuh di awal latihan, peregangan berbasis gerakan (juga dikenal sebagai dinamis) adalah yang terbaik. Artinya, segala sesuatu yang melibatkan tidak berdiri diam atau menurunkan detak jantung, misalnya lunge, walk out, gerakan yoga sederhana atau pekerjaan kardiovaskular seperti berjalan, cross trainer atau stair master.\r\n\r\n5. Jangan Lupa untuk Melakukan Pendinginan\r\n\r\nPendinginan setelah sesi olahraga sama pentingnya dengan pemanasan. Jadi, jangan sampai kamu melewatkannya, ya. Ini adalah kesempatan bagus untuk mencoba dan melepas lelah serta melemaskan beberapa area kaku yang baru saja diperjuangkan untuk dilonggarkan.\r\n\r\nBaca juga: Tips Cegah Post Workout Insomnia saat Olahraga Malam\r\n\r\n6. Jangan Membandingkan Diri dengan Orang Lain\r\n\r\nBersedialah terlihat konyol dan membuat kesalahan tanpa menghakimi diri sendiri. Teruslah mencoba, dan dengan setiap latihan kamu akan menjadi lebih baik dan lebih baik lagi. Ingat, tujuannya adalah kemajuan, bukan kesempurnaan.\r\n\r\nJadi, jangan bandingkan diri sendiri dengan orang lain yang ada di gym. Orang lain mungkin bergerak dengan mulus dan tampaknya memiliki kekuatan “manusia super”.\r\n\r\nNamun, ingatlah bahwa mereka juga pernah menjadi pemula. Jangan bandingkan bab satu kamu dengan bab sebelas orang lain, ya.\r\n\r\nItulah beberapa tips gym untuk pemula yang bisa kamu coba. Memulai olahraga di gym, seperti melakukan sesuatu yang baru, bisa sangat menegangkan. Jalanilah dengan perlahan dan bertahap, jangan terlalu keras pada diri sendiri, dan jangan sungkan minta panduan dari ahlinya.\r\n\r\nBila kamu mengalami nyeri otot atau pegal-pegal setelah berolahraga di gym, kamu bisa gunakan aplikasi Halodoc untuk beli obat atau koyo dengan mudah. Jangan lupa untuk istirahat dan minum air putih yang cukup, ya.\r\n\r\nBaca juga: 6 Gerakan Olahraga ala Gym yang Bisa Dilakukan di Rumah', '2023-08-14', 'tempat-fitness-gym-di-malang.jpeg'),
(4, '6 Gerakan Olahraga ala Gym yang Bisa Dilakukan di Rumah', '6 Gerakan Olahraga a', '', '\"Ada enam gerakan olahraga ala gym yang bisa kamu lakukan di rumah. Setiap gerakan ini bisa dilakukan dengan 3 set per gerakan, tetapi setiap gerakan disesuaikan dengan ketahan fisik tubuhmu.\"\r\n\r\nHalodoc, Jakarta – Rutin melakukan olahraga dapat memberikan berbagai dampak kesehatan pada tubuh. Dilansir dari Medline Plus, ada berbagai manfaat saat kamu rutin berolahraga, seperti menjaga kestabilan berat badan, menurunkan risiko gangguan jantung, menjaga kesehatan mental, hingga meningkatkan kualitas tidur.\r\n\r\nKini untuk rutin melakukan olahraga kamu tidak perlu menghabiskan waktu berjam-jam di pusat kebugaran, kamu bisa menikmati waktu berolahraga dengan lebih nyaman di rumah. Yuk, mulai olahraga dari gerakan ringan ala gym yang bisa dilakukan di rumah!\r\n\r\nGerakan ala Gym yang Bisa Dilakukan di Rumah\r\nDilansir dari Prevention, ada 6 gerakan olahraga ala gym yang bisa dilakukan di rumah. Setiap gerakan tergantung pada ketahanan fisik masing-masing orang, kamu bisa menyesuaikan dengan 3 set/gerakan.\r\n\r\nGerakan 1: Split Squat\r\nUntuk melakukan gerakan ini, kamu membutuhkan ayunan. Pertama, berdiri di depan ayunan lalu angkat kaki kanan dan letakkan kaki (punggung kaki tempat tali sepatu) menghadap ke dudukan ayunan.\r\n\r\nLalu, tekuk kaki kiri lebih rendah untuk melakukan single squat. Kaki kanan menjadi pijakan untuk menahan tubuh ketika kaki kiri ditekuk. Ulangi 12-15 hitungan lalu tukar dengan kaki kiri untuk diletakkan di dudukan ayunan.\r\n\r\nGerakan 2: Plank dengan Kedua Kaki\r\nGerakan ini juga membutuhkan ayunan seperti gerakan pertama. Caranya dengan berdiri di depan ayunan. Lalu, turunkan tangan ke bawah selebar bahu dan tempatkan kaki pada ayunan.\r\n\r\nPastikan punggung lurus saat kamu mengangkat kaki ke dudukan ayunan. Setelah itu, dorong lutut ke dada sambil mengangkat punggung. Kemudian, perlahan kembali ke posisi plank. Ulangi gerakan ini 12 hingga 15 kali.\r\n\r\nGerakan 3: Bangku taman\r\nUntuk melakukan gerakan ini kamu butuh kursi atau bangku panjang. Mulai dengan kedua kaki di atas bangku. Turunkan kaki kanan sedangkan kaki kiri tetap di atas bangku. Secara otomatis lutut kiri akan menekuk.\r\n\r\nKemudian, naikkan kaki kanan dan berganti menurunkan kaki kiri. Lakukan dengan tangan di pinggang. Ulangi gerakan ini 10 hingga 12 kali untuk masing-masing kaki.\r\n\r\nGerakan 4: Plank dengan Lutut hingga Dada dan Kaki\r\n\r\nUntuk melakukan gerakan ini kamu membutuhkan kursi atau bangku panjang. Pertama letakkan tangan selebar bahu pada dudukan bangku lalu tarik kaki ke belakang seperti gerakan push up.\r\n\r\nPunggung harus lurus membentuk siku-siku saat kaki direntangkan dan kedua tangan di bangku. Kemudian, tarik kaki kanan ke arah dada diantara kedua siku. Kembalikan kaki kanan perlahan ke belakang lalu rentangkan ke atas untuk melatih otot bokong. Ulangi gerakan ini 15-20 kali lalu berganti kaki kiri.\r\n\r\nGerakan 5: Lempar Bola\r\nUntuk melakukan gerakan ini kamu membutuhkan bola tangan yang tidak memantul. Pegang bola setinggi dada dan lakukan squat. Saat berdiri kembali lempar bola ke dinding atau pagar.\r\n\r\nBiarkan bola jatuh ke tanah di depanmu. Kemudian ulangi gerakannya dengan mengambil bola lalu squat lalu lempar dan jatuhkan ke tanah. Ulangi gerakan ini 15 hingga 20 kali.\r\n\r\nGerakan 6: Lempar Bola dan Net\r\n\r\nUntuk melakukan gerakan ini kamu membutuhkan bola tangan yang tidak memantul. Jika kamu sendirian angkat bola setinggi dada lalu berdiri sekitar 1,5 meter dari net lapangan tenis. Kemudian squat dan berdiri sambil melemparkan bola melewati net.\r\n\r\nLari kecil ke sisi sebelahnya lalu ulangi gerakan ini selama 10 hitungan tanpa berhenti. Jika kamu bersama teman, kamu bisa bergantian melempar bola lalu squat dan melemparnya.\r\n\r\nSelain berolahraga, jangan lupa untuk konsumsi makanan sehat dan bernutrisi agar daya tahan tubuh kamu selalu dalam keadaan yang optimal. Itulah gerakan yang bisa dilakukan agar kondisi kesehatan kamu tetap terjaga dengan baik.\r\n\r\nNamun, tetap berhati-hati dan gunakan alas kaki yang nyaman saat melakukan olahraga di rumah. Kondisi cedera yang dialami sebaiknya segera atasi dengan bertanya langsung pada dokter melalui aplikasi Halodoc agar mendapatkan penanganan yang tepat. Download Halodoc sekarang di smartphone kamu\r\n\r\n\r\nReferensi:\r\nPrevention. Diakses pada 2020. Get A Full Body Workout at The Park With These 6 Exercises\r\nMedline Plus. Diakses pada 2020. Benefits of Exercise\r\nNational Health Service UK. Diakses pada 2020. 10 Minute Workouts', '2023-08-14', 'Senam.jpg');

-- --------------------------------------------------------

--
-- Table structure for table `kehadiran`
--

CREATE TABLE `kehadiran` (
  `id` int(11) NOT NULL,
  `id_users` int(11) NOT NULL,
  `tgl_hadir` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `created_date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP,
  `status` char(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `kehadiran`
--

INSERT INTO `kehadiran` (`id`, `id_users`, `tgl_hadir`, `created_date`, `status`) VALUES
(2, 1, '2023-08-14 17:54:03', '2023-08-14 17:54:03', 'H'),
(3, 1, '2023-08-14 17:54:03', '2023-08-14 17:54:03', 'H'),
(4, 1, '2023-08-14 17:55:38', '2023-08-14 17:55:38', 'H');

-- --------------------------------------------------------

--
-- Table structure for table `kelas`
--

CREATE TABLE `kelas` (
  `id` int(11) NOT NULL,
  `nama_kelas` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `kelas`
--

INSERT INTO `kelas` (`id`, `nama_kelas`) VALUES
(2, 'Aerobic'),
(3, 'Aerobic - Pagi'),
(4, 'Aerobic - Malem');

-- --------------------------------------------------------

--
-- Table structure for table `map_instruktur`
--

CREATE TABLE `map_instruktur` (
  `id` char(8) NOT NULL,
  `id_kelas` int(11) NOT NULL,
  `created_date` date NOT NULL,
  `id_pelatih` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `map_instruktur`
--

INSERT INTO `map_instruktur` (`id`, `id_kelas`, `created_date`, `id_pelatih`) VALUES
('MI000001', 3, '2023-08-13', 2),
('MI000002', 2, '2023-08-13', 3),
('MI000003', 4, '2023-08-13', 3);

-- --------------------------------------------------------

--
-- Table structure for table `map_member`
--

CREATE TABLE `map_member` (
  `id` char(8) NOT NULL,
  `id_users` int(11) NOT NULL,
  `created_date` date NOT NULL,
  `status` char(2) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `map_member`
--

INSERT INTO `map_member` (`id`, `id_users`, `created_date`, `status`) VALUES
('MM000001', 2, '2023-08-13', 'A');

-- --------------------------------------------------------

--
-- Table structure for table `map_member_detail`
--

CREATE TABLE `map_member_detail` (
  `id` int(11) NOT NULL,
  `id_map_member` char(8) NOT NULL,
  `id_map_ins` char(8) NOT NULL,
  `tgl_map` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `map_member_detail`
--

INSERT INTO `map_member_detail` (`id`, `id_map_member`, `id_map_ins`, `tgl_map`) VALUES
(24, 'MM000001', 'MI000001', '2023-08-13'),
(25, 'MM000001', 'MI000002', '2023-08-14'),
(26, 'MM000001', 'MI000003', '2023-08-16'),
(27, 'MM000001', 'MI000003', '2023-08-15'),
(28, 'MM000001', 'MI000002', '2023-08-18');

-- --------------------------------------------------------

--
-- Table structure for table `menu`
--

CREATE TABLE `menu` (
  `id` int(11) NOT NULL,
  `title` varchar(50) NOT NULL,
  `icon` varchar(50) NOT NULL,
  `url` varchar(50) NOT NULL,
  `is_active` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `menu`
--

INSERT INTO `menu` (`id`, `title`, `icon`, `url`, `is_active`) VALUES
(1, 'Dashboard', 'fa fa-tachometer', 'dashboard', 1),
(2, 'Pengguna', 'fa fa-users', 'users', 1),
(3, 'Menu', 'fa fa-navicon', 'menu', 1),
(4, 'Pengaturan', 'fa fa-gear', 'pengaturan', 1),
(5, 'Role', 'fa fa-shield', 'role', 1),
(6, 'Role Menu', 'fa fa-address-card', 'access', 1),
(7, 'Kelas', 'fa fa-book', 'kelas', 0),
(8, 'Pelatih', 'fa fa-users', 'pelatih', 0),
(10, 'Berita', 'fa fa-file', 'berita', 0),
(11, 'Mapping Instruktur', 'fa fa-user', 'map_instruktur', 0),
(13, 'Mapping Member', 'fa fa-user', 'map_member', 0),
(14, 'Kehadiran Member', 'fa fa-file', 'kehadiran_member', 0);

-- --------------------------------------------------------

--
-- Table structure for table `pelatih`
--

CREATE TABLE `pelatih` (
  `id` int(11) NOT NULL,
  `nama_pelatih` varchar(50) NOT NULL,
  `alamat` text NOT NULL,
  `no_telp` char(12) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pelatih`
--

INSERT INTO `pelatih` (`id`, `nama_pelatih`, `alamat`, `no_telp`) VALUES
(2, 'Pelatih 1', 'Bekasi', '081280543855'),
(3, 'Pelatih 2', 'Bekasi', '0129009090');

-- --------------------------------------------------------

--
-- Table structure for table `pengaturan`
--

CREATE TABLE `pengaturan` (
  `id` int(11) NOT NULL,
  `nama_sistem` varchar(100) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `pengaturan`
--

INSERT INTO `pengaturan` (`id`, `nama_sistem`) VALUES
(1, 'Master - Gym');

-- --------------------------------------------------------

--
-- Table structure for table `role`
--

CREATE TABLE `role` (
  `id` int(11) NOT NULL,
  `role` varchar(30) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `role`
--

INSERT INTO `role` (`id`, `role`) VALUES
(1, 'Admin'),
(2, 'Member');

-- --------------------------------------------------------

--
-- Table structure for table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `nama` varchar(128) NOT NULL,
  `email` varchar(128) NOT NULL,
  `password` varchar(255) NOT NULL,
  `img` varchar(128) NOT NULL,
  `date_created` date NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `users`
--

INSERT INTO `users` (`id`, `role_id`, `nama`, `email`, `password`, `img`, `date_created`) VALUES
(1, 1, 'nurdian', 'nurdian@gmail.com', '$2y$10$/vtAfNqtGBxf8PFOafERh.BiF6u3OFNxAMDCcZ7OJyE.P1NFR509i', 'images.png', '2023-07-30'),
(2, 2, 'Joko', 'member@gmail.com', '$2y$10$wKPD3q3p4zhQrISUcHfRTeSLPQY9VyvQxilWMTxEG9MzzessAFK9O', 'images.png', '0000-00-00');

-- --------------------------------------------------------

--
-- Table structure for table `user_access_menu`
--

CREATE TABLE `user_access_menu` (
  `id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `urutan` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data for table `user_access_menu`
--

INSERT INTO `user_access_menu` (`id`, `role_id`, `menu_id`, `urutan`) VALUES
(1, 1, 1, 1),
(3, 1, 2, 2),
(4, 1, 3, 3),
(5, 1, 4, 4),
(6, 1, 5, 5),
(7, 1, 6, 6),
(8, 1, 7, 7),
(9, 1, 8, 8),
(10, 1, 9, 9),
(11, 1, 10, 10),
(12, 1, 11, 11),
(13, 1, 14, 12),
(14, 1, 13, 13);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `berita`
--
ALTER TABLE `berita`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kehadiran`
--
ALTER TABLE `kehadiran`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `kelas`
--
ALTER TABLE `kelas`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `map_instruktur`
--
ALTER TABLE `map_instruktur`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `map_member`
--
ALTER TABLE `map_member`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `map_member_detail`
--
ALTER TABLE `map_member_detail`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `menu`
--
ALTER TABLE `menu`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pelatih`
--
ALTER TABLE `pelatih`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `pengaturan`
--
ALTER TABLE `pengaturan`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `role`
--
ALTER TABLE `role`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`);

--
-- Indexes for table `user_access_menu`
--
ALTER TABLE `user_access_menu`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `berita`
--
ALTER TABLE `berita`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `kehadiran`
--
ALTER TABLE `kehadiran`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `kelas`
--
ALTER TABLE `kelas`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT for table `map_member_detail`
--
ALTER TABLE `map_member_detail`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=29;

--
-- AUTO_INCREMENT for table `menu`
--
ALTER TABLE `menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT for table `pelatih`
--
ALTER TABLE `pelatih`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT for table `pengaturan`
--
ALTER TABLE `pengaturan`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=2;

--
-- AUTO_INCREMENT for table `role`
--
ALTER TABLE `role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT for table `user_access_menu`
--
ALTER TABLE `user_access_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
