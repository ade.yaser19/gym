<!DOCTYPE html>
<html>
	<head>
		<title>REKAP KEHADIRAN</title>
		<style type="text/css">
			@font-face {
	            font-family: 'myTahoma' !important;
	            src: url("<?= base_url('assets/fonts/tahoma.ttf') ?>") !important;
	        }
	        body {
	            font-family: 'myTahoma' !important;
	        }
			.header {
				width: 100%;
				text-align: center;
				margin: 20px auto;
				font-size: 14px;
				font-weight: bold;
			}
			.header p{
				font-size: 12px;
				margin-top: 5px;
				text-transform: none;
			}
			.center{
				width: 100%;
				text-align: center;
				margin: -10px auto;
				font-size: 18px;
				font-weight: bold;
			}
			.center p{
				font-size: 14px;
				margin-top: 5px;
				text-transform: none;
				text-decoration: none;
			}

			table {
				width: 100%;
				border-collapse: collapse;
			}
			table thead tr th,
			table tbody tr td {
				padding: 4px;
			}
			table thead tr th { text-align: center; }
			
			.sign {
				width: 100%;
				margin:20px 0;
				position: relative;
				min-height: 50px;
			}
			.sign .left {
				width: 25%;
				min-height: 50px;
				float: left;
				text-align: center;
			}
			.sign .middle {
				width: 50%;
				min-height: 50px;
				float: left;
			}
			.sign .right {
				width: 25%;
				min-height: 50px;
				float: left;
				text-align: center;
			}
			.sign p.title { font-size: 16px; font-weight: bold; }
			.sign p.name { 
				font-size: 18px; 
				text-transform: uppercase; 
				border-bottom: 1px solid black; 
				padding-bottom: 4px; 
				margin-bottom: 0; 
			}
			.sign p.grade {
				font-size: 16px;
				font-weight: bold;
				margin-top: 5px;
				margin-bottom: 0;
			}
		</style>
	</head>
		<body>

		<div class="row">
			<div class="col-sm-12" style="margin-bottom: 20px;  margin-top: -40px;">
				<div class="center" style="text-transform: uppercase; font-size: 12px; font-weight: bold;">
					<h4>LAPORAN KEHADIRAN MEMBER</h4>
				</div><!-- / HEADER -->	
			</div>
		</div>
		
		<table border="0" style="font-size: 12px; margin-left: -5px;">

		    <tr>
		        <td colspan="1" style="width: 18%; border-right: none; height: 20px; padding-left: 10px;">Tanggal Cetak </td>
		        <td colspan="2" style="border-left: none; height: 20px;">: <?= date('Y-m-d') ?></td>
		    </tr>

		</table>

		<table border="1" style="font-size: 12px; text-align: justify; margin-top: 10px;">
		    <tr style="background-color: #085394;">
                <td style="text-align:center; color: white; padding: 5px;">No</td>
                <td style="text-align:center; color: white; padding: 5px;">Nama Member</td>
                <td style="text-align:center; color: white; padding: 5px;">Tanggal Hadir</td>
                <td style="text-align:center; color: white; padding: 5px;">Kelas</td>
                <td style="text-align:center; color: white; padding: 5px;">Pelatih</td>
                <td style="text-align:center; color: white; padding: 5px;">Status</td>
            </tr>
			<?php 
			$no = 1;
			foreach ($data as $r => $m):
			?>
		    <tr>
		        <td style="text-align:center;"><?= $no++ ?></td>
		        <td style="text-align:left; padding: 5px;"><?= $m['nama']; ?></td>
		        <td style="text-align:left; padding: 5px;"><?= date('Y-m-d', strtotime($m['tgl_hadir'])); ?></td>
		        <td style="text-align:left; padding: 5px;"><?= $m['nama_kelas']; ?></td>
		        <td style="text-align:left; padding: 5px;"><?= $m['nama_pelatih']; ?></td>
		        <td style="text-align:center; padding: 5px;"><?= $m['status']; ?></td>
		    </tr>
			<?php endforeach ?>
		</table>
	</body>
</html>