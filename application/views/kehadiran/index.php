<?= $this->session->flashdata('message'); ?>
<div class="box">
    
    <div class="box-body">
        <form method="POST">
            <div class="box-body col-xs-12">
                <div class="row">
                    <div class="form-group">
                        <div class="col-sm-5">
                            <label for="tanggal">Tanggal Awal</label>
                              <input type="date" class="form-control" name="tanggal_awal" value="<?php echo $tanggal_awal = $awal != null ? $awal : "" ?>">
                        </div>

                        <div class="col-sm-5">
                            <label for="tanggal">Tanggal Akhir</label>
                              <input type="date" class="form-control" name="tanggal_akhir" value="<?php echo $tanggal_akhir = $akhir != null ? $akhir : "" ?>">
                        </div>
                    </div>
                 </div> 

                <div class="row" style="margin-top: 10px; float: left;">
                    <div class="form-group">
                        <div class="col-sm-12">
                          <input type="submit" name="submit" value="Tampilkan" class="btn btn-sm btn-flat btn-primary">
                          <a href="<?= base_url('Kehadiran_member'); ?>" class="btn btn-sm btn-flat btn-warning" style="margin: 2px;">Reset</a>
                          <a href="<?= base_url('Kehadiran_member/pdf_data'); ?>" class="btn btn-sm btn-flat btn-success" style="margin: 2px;" target="_blank">Cetak PDF</a>

                          <a href="<?= base_url('front_page/kehadiran'); ?>" class="btn btn-sm btn-flat btn-info" style="margin: 2px;" target="_blank"><i class="glyphicon glyphicon-qrcode"></i> Scan QR Kehadiaran</a>

                        </div>
                    </div>
                </div>

            </div>
        </form>
    </div>

    <div class="box-body">
        <div class="table-responsive">
        <table id="example1" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th style="text-align: center; width:5px;">No</th>
                    <th>Nama Member</th>
                    <th>Tanggal Hadir</th>
                    <th>Kelas</th>
                    <th>Pelatih</th>
                    <th>Status</th>
                </tr>
            </thead>
            <tbody>
                <?php

                $x = 1;
                foreach ($rekap as $key => $m) : ?>
                    <tr>
                        <td style="text-align: center;"><?= $x++; ?></td>
                        <td><?= $m['nama']; ?></td>
                        <td><?= date('Y-m-d', strtotime($m['tgl_hadir'])); ?></td>
                        <td><?= $m['nama_kelas']; ?></td>
                        <td><?= $m['nama_pelatih']; ?></td>
                        <td><?= $m['status']; ?></td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
       </div>
    </div>
    <!-- /.box-body -->
</div>
<!-- /.box -->

