<!DOCTYPE html>
<html>
    <head>
        <!-- Site made with Mobirise Website Builder v5.8.14, https://mobirise.com -->
        <meta charset="UTF-8">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <meta name="generator" content="Mobirise v5.8.14, mobirise.com">
        <meta name="viewport" content="width=device-width, initial-scale=1, minimum-scale=1">
      <!--   <link rel="shortcut icon" href="assets/images/logo5.png" type="image/x-icon"> -->
        <meta name="description" content="">
        <title>index</title>
        <link rel="stylesheet" href="<?=base_url("assets/front_end/bootstrap/css/bootstrap.min.css")?>">
        <link rel="stylesheet" href="<?=base_url("assets/front_end/bootstrap/css/bootstrap-grid.min.css")?>">
        <link rel="stylesheet" href="<?=base_url("assets/front_end/bootstrap/css/bootstrap-reboot.min.css")?>">
        <link rel="stylesheet" href="<?=base_url("assets/front_end/theme/css/style.css")?>">
        <link rel="preload" href="https://fonts.googleapis.com/css2?family=Jost:ital,wght@0,400;0,700;1,400;1,700&display=swap&display=swap" as="style" onload="this.onload=null;this.rel='stylesheet'">
        <noscript>
            <link rel="stylesheet" href="https://fonts.googleapis.com/css2?family=Jost:ital,wght@0,400;0,700;1,400;1,700&display=swap&display=swap">
        </noscript>
        <link rel="preload" as="style" href="<?=base_url("assets/front_end/mobirise/css/mbr-additional.css")?>">
        <link rel="stylesheet" href="<?=base_url("assets/front_end/mobirise/css/mbr-additional.css")?>" type="text/css">
    </head>
    <body>
        <section data-bs-version="5.1" class="header4 cid-tMUQPfZlzv mbr-fullscreen" id="header4-1">
            <div class="container">
                <div class="row">
                    <div class="content-wrap">
                        <h1 class="mbr-section-title mbr-fonts-style mbr-white mb-3 display-1">
                            <strong>Mulai Hidup Sehat Dan Bugar</strong>
                            <br>
                            <strong>Dengan Gym</strong>
                        </h1>
                        <p class="mbr-fonts-style mbr-text mbr-white mb-3 display-7">Daftarkan Sekarang , Dan Temukan Ke Nikmatan Hidup Sehat</p>
                        <div class="mbr-section-btn">
                            <a class="btn btn-primary display-4" href="<?=base_url("auth")?>">Login</a>
                        </div>
                    </div>
                </div>
            </div>
        </section>
<!-- SPLIT ARTIKEL -->

     <?php 
        // echo "<pre>";
        // print_r($berita);die;
     foreach($berita as $berita){?>

        <section data-bs-version="5.1" class="image1 cid-tMUWwZXyk1 mbr-fullscreen" id="image1-2">
            <div>
                <div class="row align-items-center">
                    <div class="col-12 col-lg-7">
                        <div class="image-wrapper">
                            <img src="<?=base_url("uploads/berita/").$berita['foto']?>">
                            <p class="mbr-description mbr-fonts-style pt-2 align-center display-4"></p>
                        </div>
                    </div>
                    <div class="col-12 col-lg-4">
                        <div class="text-wrapper">
                            <h3 class="mbr-section-title mbr-fonts-style mb-1 display-5">
                                <strong><?=$berita['judul_berita'];?></strong>
                            </h3>
                             <h5 class="mbr-section-title mbr-fonts-style mb-1 display-9">
                               <b> Posting Date  :  <?php tglIndonesia($berita['created_date']);?></b>
                            </h5>
                            <h5 class="mbr-section-title mbr-fonts-style mb-1 display-9">
                                <b>Tipe Berita  :  <?=$berita['tipe_berita']?></b>
                            </h5>
                            <p class="mbr-text mbr-fonts-style display-7"><?=$berita['isi_berita'];?> </p>
                        </div>
                    </div>
                </div>
            </div>
        </section>
    <?php }?>

<!-- SPLIT ARTIKEL -->
        <section data-bs-version="5.1" class="content6 cid-tMUWE7xnxl" id="content6-5">

        <div class="container">
            <div class="row justify-content-center">
                <div class="col-md-12 col-lg-10">
                    <hr class="line">
                      <p class="mbr-text align-center mbr-fonts-style my-4 display-5"><em>Kesehatan Adalah Kunci Menuju Kehamonisan.</em></p>
                     <hr class="line">
                </div>
            </div>
        </div>

    </section>




    <script src="<?=base_url("assets/front_end/bootstrap/js/bootstrap.bundle.min.js")?>"></script>
    <script src="<?=base_url("assets/front_end/smoothscroll/smooth-scroll.js")?>"></script>
    <script src="<?=base_url("assets/front_end/ytplayer/index.js")?>"></script>
    <script src="<?=base_url("assets/front_end/theme/js/script.js")?>"></script>
    
    </body>
</html>