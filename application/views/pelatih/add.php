<?= validation_errors(
    '<div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>',
    '</div>'
); ?>

<div class="box">
    <div class="box-header">
        <h3 class="box-title">Tambah Pelatih</h3>
    </div>
    <div class="box-body">
        <form action="<?= base_url('Pelatih/add'); ?>" class="form" method="POST">
            <div class="form-group">
                <label>Nama Pelatih</label>
                <input type="text" name="nama_pelatih" class="form-control" placeholder="Nama Pelatih">
            </div>
            <div class="form-group">
                <label>Alamat</label>
                <input type="text" name="alamat" class="form-control" placeholder="Alamat">
            </div>
            <div class="form-group">
                <label>No Telepon</label>
                <input type="text" name="no_telp" class="form-control" placeholder="No Telepon">
            </div>
            <a href="<?= base_url('Pelatih') ?>" class="btn btn-sm btn-warning" style="float: right;  margin-left: 5px;">Kembali</a>
            <button class="btn btn-sm btn-primary" type="submit" style="float: right;">Simpan</button>
        </form>
    </div>
</div>