<?= $this->session->flashdata('message'); ?>
<div class="box">
    <div class="box">
        <div class="box-body">
            <a href="<?= base_url('Pelatih/add'); ?>" class="btn btn-sm btn-flat btn-primary"><i class="glyphicon glyphicon-plus"></i> Tambah</a>
        </div>
    </div>
    <div class="box-body">
        <table id="example1" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th style="text-align: center; width:5px;">No</th>
                    <th>Nama Pelatih</th>
                    <th>Alamat</th>
                    <th>No Telepon</th>
                    <th style="text-align: center;">Aksi</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $x = 1;
                foreach ($pelatih as $v) : ?>
                    <tr>
                        <td style="text-align: center;"><?= $x++; ?></td>
                        <td><?= $v['nama_pelatih']; ?></td>
                        <td><?= $v['alamat']; ?></td>
                        <td><?= $v['no_telp']; ?></td>
                        <td style="text-align: center;">
                            <a href="<?= base_url('Pelatih/edit/') . $v['id']; ?>" class="btn btn-flat btn-xs  btn-warning"><i class="glyphicon glyphicon-pencil"></i></a>
                            <a onclick="return confirm('Hapus ?' );" href="<?= base_url('Pelatih/delete/') . $v['id']; ?>" class="btn btn-flat btn-xs  btn-danger"><i class="glyphicon glyphicon-remove"></i></a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <!-- /.box-body -->
</div>
<!-- /.box -->