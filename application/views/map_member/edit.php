<?= validation_errors(
    '<div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>',
    '</div>'
); ?>

<div class="box">
    <div class="box-body">
        <form action="<?= base_url('Map_member/edit'); ?>" class="form" method="POST">
            
            <div class="form-group">
                <input type="hidden" name="id_map_member" class="form-control" readonly="true" placeholder="Map Member" value="<?= $id ?>">
            </div>

            <div class="form-group">
                <label>Member</label>
                <select name="id_users" class="form-control">
                  <option value="">- Pilih -</option>
                  <?php foreach ($users as $value) { ?>
                  <option value="<?php echo $value->id ?>" <?=$id_users == $value->id ? "selected":""?>>
                  <?php echo $value->nama ?>
                  </option>
                  <?php } ?>
                </select>
            </div>

            <div class="form-group">
                <label>Status</label>
                <select name="status" class="form-control">
                  <option value="">- Pilih -</option>
                  <option value="A" <?=$status == 'A' ? "selected":""?>>Aktif</option>
                  <option value="TA" <?=$status == 'TA' ? "selected":""?>>Tidak Aktif</option>
                </select>
            </div>

            <button type="button" class="btn btn-primary" data-toggle="modal" onclick="add_form()">
            Tambah Paket</button>
            <hr style="height:2px; width:100%; border-width:0; color:green; background-color:green">
            
            <div class="row">
                <div class="col-sm-12">
                    <table id="data-paket" class="table table-bordered table-hover">
                        <thead>
                        <tr>
                              <th width="5%"><center>No</center></th>
                              <th><center>Kode Map</center></th>
                              <th><center>Nama Kelas</center></th>
                              <th><center>Pelatih</center></th>
                              <th><center>Tanggal</center></th>
                              <th><center>Action</center></th>
                            </center>
                        </tr>
                        </thead>
                        <tbody>
                          <tr><td colspan="6"><center>Data kosong</center></td></tr>
                        </tbody>
                  </table>
                </div>
            </div>
            <input type="hidden" name="data_paket" id="data_paket" value='<?php echo json_encode($map_member_detail) ?>'>
            <a href="<?= base_url('Map_member') ?>" class="btn btn-sm btn-warning" style="float: right;  margin-left: 5px;">Kembali</a>
            <button class="btn btn-sm btn-primary" type="submit" style="float: right;">Simpan</button>
        </form>
    </div>
</div>

<div class="modal fade" id="form_paket" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
      <div class="modal-header">
        <h5 class="modal-title" id="exampleModalLabel">Tambah Paket</h5>
        <button type="button" class="close" data-dismiss="modal" aria-label="Close">
          <span aria-hidden="true">&times;</span>
        </button>
      </div>
      <div class="modal-body">
        <table class="table table-sm">
         <tr>
            <td>Kode Paket</td>
            <td>:</td>
            <td>
                <select class="form-control" name="id_map_ins" id="id_map_ins" onchange="kode()">
                  <option value="">- Pilih -</option>
                  <?php foreach ($inst as $row => $value) : ?>
                  <option value="<?php echo $value->id ?>"><?php echo $value->id ?></option>
                  <?php endforeach ?>
                </select>
            </td>
        </tr>
        <tr>
            <td>Nama Kelas</td>
            <td>:</td>
            <td><input type="text" class="form-control" name="nama_kelas" id="nama_kelas" readonly="true"></td>
        </tr>
        <tr>
            <td>Pelatih</td>
            <td>:</td>
            <td><input type="text" class="form-control" name="nama_pelatih" id="nama_pelatih" readonly="true"></td>
        </tr>
        <tr>
            <td>Tanggal</td>
            <td>:</td>
            <td><input type="date" class="form-control" name="tgl_map" id="tgl_map"></td>
        </tr>
        </table>
      </div>
      <div class="modal-footer">
        <button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
        <button type="button" class="btn btn-primary" id="form_data">Save changes</button>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript">

    var simpan_barang =($('#data_paket').val() != "")? $.parseJSON($('#data_paket').val()): [];
    var inst = '<?php echo json_encode($inst);?>';

    $(document).ready(function() {
        $('#form_paket').on('hidden.bs.modal', function () {
            bersihkan_form();
            $('#form_data').unbind('click');
        });
        render_table();
    });

    function kode() {    
        var id_map_ins = $("#id_map_ins").val();
        $.ajax({
            url : "<?php echo base_url();?>/Map_member/get_kode_map",
            method : "POST",
            data : {id:id_map_ins},
            async : false,
            dataType : 'json',
            success: function(data){
                document.getElementById("nama_kelas").value = data[0]['nama_kelas'];
                document.getElementById("nama_pelatih").value = data[0]['nama_pelatih'];
            }
        });
    }

    // -----------------------------------------PROSES Simpan
  function tambah_barang(){
    var paket;
    var id_map_ins      = $('#id_map_ins').val();
    var nama_kelas  = $('#nama_kelas').val();
    var tgl_map  = $('#tgl_map').val();
    var nama_pelatih         = $('#nama_pelatih').val();
    var obj = $.parseJSON(inst);
    $.each(obj, function(index, val) {
      if (id_map_ins == val.id){
        barang = val;
      }
    });

    simpan_barang.push({
      id_map_ins:id_map_ins,
      nama_kelas: nama_kelas,
      nama_pelatih:nama_pelatih,
      tgl_map:tgl_map
    });

    simpan_data();
    render_table();
    $('#form_paket').modal('hide');
  }

  // ---------------------------------------------FORM Add
  function add_form(){
    $('#form_data').bind('click', function(event) {
      tambah_barang();
    });
    $('#form_paket').modal('show');
  }

   // ------------------------------------------CLEAR FORM DIMODAL
  function bersihkan_form(){
   // Clear pilih data di form
      $('#id_map_ins').val('');
      $('#nama_kelas').val('');
      $('#nama_pelatih').val('');
      $('#tgl_map').val('');
  }

  // ------------------------------------------REFRESH TAMPILAN
  function render_table(){
    $('#data-paket').find('tbody').empty();
    $.each(simpan_barang, function(index, val) {
      var html    = '<tr id="'+ index +'">';
      html        += '<td><center>'+ (index+1) +'</center></td>';
      html        += '<td><center>'+ val.id_map_ins +'</center></td>';
      html        += '<td><center>'+ val.nama_kelas +'</center></td>';
      html        += '<td><center>'+ val.nama_pelatih +'</center></td>';
      html        += '<td><center>'+ val.tgl_map +'</center></td>';
      html        += '<td><center><button type="button" title="Delete" class="btn btn-danger btn-sm" onclick="hapus_barang('+ index +')"><i class="fa fa-trash"></i> Hapus</button></center></td>';
      html        += '</tr>';
      $('#data-paket').find('tbody').append(html);
    });
    // Jika panjang data barang == 0 maka
    if (simpan_barang.length == 0) {
      // menampilkan tulisan yang ada didalam tbody
      var html = '<tr><td colspan="6"><center>Data kosong</center></td></tr>';
      // Lalu Masukan tampilan yang ada sesuai dengan tbody
      $('#data-paket').find('tbody').append(html);
    }
  }

  // ------------------------------------------INJEK KE DATABASE
  function simpan_data(){
    // dari data yang sudah ada didalam variabel data_barang yang awalnya object dirubah menjadi string lalu ditampung kedalam variabel simpan_barang
    $('#data_paket').val(JSON.stringify(simpan_barang));
  }

  // ----------------------------------------------Hapus Temporary Data
  function hapus_barang(id){
    if (confirm('Apakah anda yakin menghapus data ini ?')) {
      simpan_barang.splice(id, 1);
      simpan_data();
      render_table();
    }
  }

</script>