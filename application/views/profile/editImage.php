<?= validation_errors(
    '<div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>',
    '</div>'
); ?>

<?= $this->session->flashdata('message'); ?>

<div class="box">
    <div class="box-header">
        <h3 class="box-title">Edit gambar profile</h3>
    </div>
    <div class="box-body">
        <img class="profile-user-img img-responsive img-circle" src="<?= base_url('assets/img/profile/') . $img; ?>" alt="User profile picture">

        <?php echo form_open_multipart('Profile/image'); ?>
        <div class="form-group">
            <label for="image">Upload gambar</label>
            <input type="file" name="image" id="image" class="form-control" required>
        </div>
        <a href="<?= base_url('Profile') ?>" class="btn btn-sm btn-warning" style ="float: right; margin-left: 5px;">Kembali</a>
        <button type="submit" class="btn btn-sm btn-primary" style="float:right;">Simpan</button>
        </form>
    </div>
</div>