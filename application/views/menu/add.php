<?= validation_errors(
	'<div class="alert alert-danger alert-dismissible">
	<button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>',
	'</div>'
); ?>


<div class="box">
	<div class="box-header">
		<h3>Tambah Menu</h3>
	</div>
	<div class="box-body">
		<form action="<?= base_url('Menu/add'); ?>" class="form" method="POST">
			
			<div class="form-group">
				<label>Nama Menu</label>
				<input type="text" class="form-control" name="title" value="">
			</div>
			
			<div class="form-group">
				<label>Icon Menu</label>
				<input type="text" class="form-control" name="icon" value="">
			</div>
			
			<div class="form-group">
				<label>Url Menu</label>
				<input type="text" class="form-control" name="url" value="">
			</div>
			
			<a href="<?= base_url('Menu') ?>" class="btn btn-sm btn-warning" style ="float: right; margin-left: 5px;">Kembali</a>
			<button type="submit" class="btn btn-sm btn-primary" style ="float: right;">Simpan</button>
		
		</form>	
	</div>
	
</div>