<?= validation_errors(
    '<div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>',
    '</div>'
); ?>

<div class="box">
    <div class="box-header">
        <h3 class="box-title">Tambah Berita</h3>
    </div>
    <div class="box-body">
        <form action="<?= base_url('Berita/add'); ?>" class="form" method="POST" enctype="multipart/form-data">
            
            <div class="form-group">
                <label>Judul</label>
                <input type="text" name="judul_berita" class="form-control" placeholder="Judul">
            </div>

            <div class="form-group">
                <label>Tipe Berita</label>
                <select name="tipe_berita" class="form-control">
                  <option value="">- Pilih -</option>
                  <option value="P">Publish</option>
                  <option value="TP">Tidak Publish</option>
                </select>
            </div>

            <div class="form-group">
                <label>Status Berita</label>
                <select name="status_berita" class="form-control">
                  <option value="">- Pilih -</option>
                  <option value="A">Aktif</option>
                  <option value="T">Tidak Aktif</option>
                </select>
            </div>

            <div class="form-group">
                <label>Isi</label>
                <input type="text" name="isi_berita" class="form-control" placeholder="Isi">
            </div>

            <div class="form-group">
                <label>Tanggal Dibuat</label>
                <input type="date" name="created_date" class="form-control" placeholder="Tanggal Dibuat">
            </div>

            <div class="form-group">
                <label>Gambar</label>
                <input type="file" name="image" class="form-control" placeholder="Gambar">
            </div>

            <a href="<?= base_url('Berita') ?>" class="btn btn-sm btn-warning" style="float: right;  margin-left: 5px;">Kembali</a>
            <button class="btn btn-sm btn-primary" type="submit" style="float: right;">Simpan</button>
        </form>
    </div>
</div>