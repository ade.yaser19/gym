<?= $this->session->flashdata('message'); ?>
<div class="box">
    <div class="box">
        <div class="box-body">
            <a href="<?= base_url('Map_instruktur/add'); ?>" class="btn btn-sm btn-flat btn-primary"><i class="glyphicon glyphicon-plus"></i> Tambah</a>
        </div>
    </div>
    <div class="box-body">
        <table id="example1" class="table table-bordered table-striped">
            <thead>
                <tr>
                    <th style="text-align: center; width:5px;">No</th>
                    <th>Nama Kelas</th>
                    <th>Pelatih</th>
                    <th style="text-align: center;">Aksi</th>
                </tr>
            </thead>
            <tbody>
                <?php
                $x = 1;
                foreach ($map_instruktur as $v) : ?>
                    <tr>
                        <td style="text-align: center;"><?= $x++; ?></td>
                        <td><?= $v['nama_kelas']; ?></td>
                        <td><?= $v['nama_pelatih']; ?></td>
                        <td style="text-align: center;">
                            <a href="<?= base_url('Map_instruktur/edit/') . $v['id']; ?>" class="btn btn-flat btn-xs  btn-warning"><i class="glyphicon glyphicon-pencil"></i></a>
                            <a onclick="return confirm('Hapus ?' );" href="<?= base_url('Map_instruktur/delete/') . $v['id']; ?>" class="btn btn-flat btn-xs  btn-danger"><i class="glyphicon glyphicon-remove"></i></a>
                        </td>
                    </tr>
                <?php endforeach; ?>
            </tbody>
        </table>
    </div>
    <!-- /.box-body -->
</div>
<!-- /.box -->