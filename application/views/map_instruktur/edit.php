<?= validation_errors(
    '<div class="alert alert-danger alert-dismissible">
            <button type="button" class="close" data-dismiss="alert" aria-hidden="true">×</button>',
    '</div>'
); ?>

<div class="box">
    <div class="box-body">
        <form action="<?= base_url('Map_instruktur/edit'); ?>" class="form" method="POST">
            <input type="hidden" name="id" value="<?= $id ?>">
            <div class="form-group">
                <label>Nama Kelas</label>
                <select name="id_kelas" class="form-control">
                  <option value="">- Pilih -</option>
                  <?php foreach ($kelas as $value) { ?>
                  <option value="<?php echo $value->id ?>" <?=$id_kelas == $value->id ? "selected":""?>>
                  <?php echo $value->nama_kelas ?>
                  </option>
                  <?php } ?>
                </select>
            </div>

            <div class="form-group">
                <label>Pelatih</label>
                <select name="id_pelatih" class="form-control">
                  <option value="">- Pilih -</option>
                  <?php foreach ($pelatih as $value) { ?>
                  <option value="<?php echo $value->id ?>" <?=$id_pelatih == $value->id ? "selected":""?>>
                  <?php echo $value->nama_pelatih ?>
                  </option>
                  <?php } ?>
                </select>
            </div>

            <a href="<?= base_url('Map_instruktur') ?>" class="btn btn-sm btn-warning" style="float: right;  margin-left: 5px;">Kembali</a>
            <button class="btn btn-sm btn-primary" type="submit" style="float: right;">Simpan</button>
        </form>
    </div>
</div>