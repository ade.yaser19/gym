<div class="box">
    <div class="box-header">
        <h3 class="box-title">Edit Pengguna</h3>
    </div>
    <div class="box-body">
        <?= validation_errors(); ?>
        <form action="<?= base_url('Users/edit/'); ?>" method="post" class="form">
            <input type="hidden" name="id" value="<?= $id; ?>">
            
            <div class="form-group">
                <label for="nama">Nama</label>
                <input type="text" class="form-control" name="name" value="<?= $nama; ?>" required>
            </div>
            
            <div class="form-group">
                <label for="email">Email</label>
                <input type="text" class="form-control" name="email" value="<?= $email; ?>" required>
            </div>
            
            <div class="form-group">
                <label for="password">Password</label>
                <input type="password" class="form-control" name="password" value="" required>
            </div>
            
            <div class="form-group">
                <label>Hak Akses</label>
                <select name="role_id" class="form-control select2">
                  <option value="">- Pilih -</option>
                  <?php foreach ($role as $value) { ?>
                  <option data-role ="<?php echo $role_id ?>" value="<?php echo $value->id ?>" <?=$role_id == $value->id ? "selected":""?>>
                  <?php echo $value->role ?>
                  </option>
                  <?php } ?>
                </select>
            </div>

            <a href="<?= base_url('Users') ?>" class="btn btn-sm btn-warning" style="float: right; margin-left: 5px;">Kembali</a>
            <button class="btn btn-sm btn-primary" type="submit" style="float: right;">Ubah</button>
        </form>
    </div>
</div>