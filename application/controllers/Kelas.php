<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Kelas extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        is_logged_in();
        $this->load->model('Kelas_model', 'kelas');
    }

    public function index()
    {
        $user = $this->db->get_where('users', ['email' => $this->session->userdata('email')])->row_array();
        $name = $user['nama'];
        $img  = $user['img'];
        $date_created = $user['date_created'];
        $data = [
            'head'          => 'Kelas',
            'name'          => $name,
            'img'           => $img,
            'date_created'  => $date_created
        ];

        $data['kelas'] = $this->db->get('kelas')->result_array();

        $this->load->view('templates/head', $data);
        $this->load->view('templates/nav', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('kelas/index', $data);
        $this->load->view('templates/footer');
    }

    public function add()
    {
        $post = $this->input->post();
        if ($post) {            
            $kelas = [
                'nama_kelas' => $this->input->post('nama_kelas')
            ];
            $this->db->insert('kelas', $kelas);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
                Data berhasil disimpan
                </div>');
            redirect('Kelas');
        }

        $user = $this->db->get_where('users', ['email' => $this->session->userdata('email')])->row_array();
        $name = $user['nama'];
        $img  = $user['img'];
        $date_created = $user['date_created'];
        $data = [
            'title'         => 'Dashboard',
            'head'          => 'Kelas',
            'name'          => $name,
            'img'           => $img,
            'date_created'  => $date_created
        ];
        $this->load->view('templates/head', $data);
        $this->load->view('templates/nav', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('kelas/add');
        $this->load->view('templates/footer');
    }

    public function edit()
    {

        $post = $this->input->post();

        if ($post) {
            $data = [
                'nama_kelas' => $this->input->post('nama_kelas')
            ];
            $this->kelas->update($data);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
                Data berhasil diubah
                </div>');
            redirect('Kelas');
        }

        $user = $this->db->get_where('users', ['email' => $this->session->userdata('email')])->row_array();
        $name = $user['nama'];
        $img  = $user['img'];
        $date_created = $user['date_created'];
        $data = [
            'title'         => 'Dashboard',
            'head'          => 'Kelas',
            'name'          => $name,
            'img'           => $img,
            'date_created'  => $date_created
        ];

        $get = $this->kelas->detail();
        $this->load->view('templates/head', $data);
        $this->load->view('templates/nav', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('kelas/edit', $get);
        $this->load->view('templates/footer');
    }

    public function delete()
    {
        $id = $this->uri->segment(3);
        $this->kelas->hapus();
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            Data berhasil dihapus
            </div>');
        redirect('Kelas');
    }
}
