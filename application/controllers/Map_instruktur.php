<?php 
defined('BASEPATH') or exit('No direct script access allowed');

class Map_instruktur extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->model('Map_inst_model', 'map_inst');
    }

    public function index()
    {
        $user           = $this->db->get_where('users', ['email' => $this->session->userdata('email')])->row_array();
        $name           = $user['nama'];
        $img            = $user['img'];
        $date_created   = $user['date_created'];
        $data = [
            'head'          => 'Mapping Instuktur',
            'name'          => $name,
            'img'           => $img,
            'date_created'  => $date_created
        ];

        $data['map_instruktur'] = $this->map_inst->get_map_inst($this->session->userdata('user_id'));
        
        $this->load->view('templates/head', $data);
        $this->load->view('templates/nav', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('map_instruktur/index', $data);
        $this->load->view('templates/footer');
    }

    public function add()
    {
        $post = $this->input->post();
        
        if ($post) {            
            
            $check_data = $this->map_inst->check_map_instruktur($post['id_kelas'], $post['id_pelatih']);
            
            if ($check_data == '') {
            
                $data = array   (   'id'  			=>  $post['id'],
                					'id_kelas'  	=>  $post['id_kelas'],
                                    'id_pelatih'    =>  $post['id_pelatih'],
                                    'created_date' 	=>  date('Y-m-d'),
                                );
                $this->map_inst->tambah($data);

                $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
                Data berhasil ditambah
                </div>');
                redirect('Map_instruktur');
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
                Data sudah ada
                </div>');
                redirect('Map_instruktur');
            }
        }

        $user       = $this->db->get_where('users', ['email' => $this->session->userdata('email')])->row_array();
        $name       = $user['nama'];
        $img        = $user['img'];
        $date_created = $user['date_created'];
        $kelas      = $this->map_inst->get_kelas();
        $pelatih    = $this->map_inst->get_pelatih();
        $kode       = $this->map_inst->CreateCode();

        $data = [
            'title'         => 'Dashboard',
            'head'          => 'Mapping Instuktur',
            'name'          => $name,
            'img'           => $img,
            'date_created'  => $date_created,
            'kelas'  		=> $kelas,
            'pelatih'       => $pelatih,
            'kode'          => $kode
        ];
        $this->load->view('templates/head', $data);
        $this->load->view('templates/nav', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('map_instruktur/add');
        $this->load->view('templates/footer');
    }

    public function edit()
    {
        $post = $this->input->post();

        if ($post) {

        	$check_data = $this->map_inst->check_map_instruktur($post['id_kelas'], $post['id_pelatih']);

            if ($check_data == FALSE) {

            $data = array   (   
                                'id_kelas'   	=>  $post['id_kelas'],
                                'id_pelatih'    =>  $post['id_pelatih'],
                            );
            $this->map_inst->update($data);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            Data berhasil dirubah
            </div>');
            redirect('Map_instruktur');
            } else {
                $this->session->set_flashdata('message', '<div class="alert alert-danger" role="alert">
                Data sudah ada
                </div>');
                redirect('Map_instruktur');
            }
        }

        $user 			= $this->db->get_where('users', ['email' => $this->session->userdata('email')])->row_array();
        $name 			= $user['nama'];
        $img  			= $user['img'];
        $date_created   = $user['date_created'];
        $id    			= $this->uri->segment(3);
        $kelas 			= $this->map_inst->get_kelas();
        $pelatih        = $this->map_inst->get_pelatih();

        $map_instruktur = $this->map_inst->detail($id);

        $data = [
            'title'         => 'Order',
            'head'          => 'Edit Mapping Instuktur',
            'name'          => $name,
            'img'           => $img,
            'date_created'  => $date_created,
            'kelas'         => $kelas,
            'pelatih'        => $pelatih
        ];

        $this->load->view('templates/head', $data);
        $this->load->view('templates/nav', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('map_instruktur/edit', $map_instruktur);
        $this->load->view('templates/footer');
    }

   	public function delete()
    {
        $id = $this->uri->segment(3);
        $this->map_inst->hapus();
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            Data berhasil dihapus
            </div>');
        redirect('Map_instruktur');
    }

}

/* End of file Map_instruktur.php */
/* Location: ./application/controllers/Map_instruktur.php */