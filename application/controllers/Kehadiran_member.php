<?php 

defined('BASEPATH')or exit('No direct script access allowed');

class Kehadiran_member extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->model('Rekap_model', 'rekap');
    }

	public function index()
    {
        $user           = $this->db->get_where('users', ['email' => $this->session->userdata('email')])->row_array();
        $name           = $user['nama'];
        $img            = $user['img'];
        $date_created   = $user['date_created'];
        $tanggal_awal   ='';
        $tanggal_akhir  ='';
        $data = [
            'head'          => '<i class="fa fa-file"> Rekap Kehadiran</i>',
            'name'          => $name,
            'img'           => $img,
            'role'          => $user['role_id'],
            'date_created'  => $date_created,
            'awal'          => $this->input->post('tanggal_awal'),
            'akhir'         => $this->input->post('tanggal_akhir')
        ];

        if(isset($_POST['tanggal_awal']) && $_POST['tanggal_awal'] !=''){
            $tanggal_awal = $_POST['tanggal_awal'];
        }

        if(isset($_POST['tanggal_akhir']) && $_POST['tanggal_akhir'] !=''){
            $tanggal_akhir = $_POST['tanggal_akhir'];
        }

        $data['rekap'] = $this->rekap->get($tanggal_awal, $tanggal_akhir);

        foreach ($data['rekap'] as $r => $v) {

        	if ($v['status'] == 'H') {
        		$data['rekap'][$r]['status'] = 'Hadir';
        	} else {
        		$data['rekap'][$r]['status'] = 'Tidak Hadir';
        	}
        }

        $this->session->set_userdata([
            'REPORT_KEHADIRAN' => $data['rekap'],
        ]);

        $this->load->view('templates/head', $data);
        $this->load->view('templates/nav', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('kehadiran/index',$data);
        $this->load->view('templates/footer');
    }

    public function pdf_data()
    {
        $data = $this->session->userdata('REPORT_KEHADIRAN');
        
         if ($data) {
            $html = $this->load->view('kehadiran/laporan', [
                'data'  => $data
            ], true);
            $this->load->library('Pdf');
            $this->pdf->pdf->AddPage('P');
            $this->pdf->pdf->WriteHTML($html);
            $this->pdf->pdf->Output('Laporan Kehadiran Member'.'.pdf', 'I');
        } else {
            redirect('Kehadiran_member');
        }
    }

     public function add_kehadiran()
    {
        $param = array('id_users'=>$this->session->userdata('user_id'),'status'=>'H');
        $this->rekap->add_kehadiran($param);
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
        Apsen Member Berhasil
        </div>');
       redirect('Kehadiran_member');
    }


}

/* End of file Kehadiran_member.php */
/* Location: ./application/controllers/Kehadiran_member.php */