<?php
defined('BASEPATH') OR exit('No direct script access allowed');

class Front_page extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->model('Berita_model', 'berita');
    }

	public function index()
	{
	    $data['berita'] = $this->db->get('berita')->result_array();
        foreach ($data['berita'] as $r => $v) {
            
            if ($v['tipe_berita'] == 'P') {
                $data['berita'][$r]['tipe_berita'] = 'Publish';
            } else {
                $data['berita'][$r]['tipe_berita'] = 'Tidak Publish';
            }

            if ($v['status_berita'] == 'A') {
                $data['berita'][$r]['status_berita'] = 'Aktif';
            } else {
                $data['berita'][$r]['status_berita'] = 'Tidak Aktif';
            }

        }
		$this->load->view('front_page/index.php',$data);
	}

	public function kehadiran(){
		$this->load->view('scan_qr/index.php');
	}
}
