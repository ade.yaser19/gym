<?php 
defined('BASEPATH')or exit('No direct script access allowed');

class Map_member extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        $this->load->model('Map_member_model', 'map_member');
        $this->load->model('Map_inst_model', 'map_inst');
    }

    public function get_kode_map()
    {
        $id     = $this->input->post('id');
        $data   = $this->map_member->get_map_inst_byId($id);
        echo json_encode($data);
    }

    public function index()
    {
        $user           = $this->db->get_where('users', ['email' => $this->session->userdata('email')])->row_array();
        $name           = $user['nama'];
        $img            = $user['img'];
        $date_created   = $user['date_created'];
        $data = [
            'head'          => 'Mapping Member',
            'name'          => $name,
            'img'           => $img,
            'date_created'  => $date_created
        ];

        $data['map_member'] = $this->map_member->get_map_member($this->session->userdata('user_id'));
       
        $this->load->view('templates/head', $data);
        $this->load->view('templates/nav', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('map_member/index', $data);
        $this->load->view('templates/footer');
    }

    public function add()
    {

        $post = $this->input->post();

        if ($post) {
            $data = array   (   'id'           =>  $post['id_map_member'],
                                'id_users'     =>  $post['id_users'],
                                'status'       =>  $post['status'],
                                'created_date' =>  date('Y-m-d')
                            );
            $this->map_member->tambah_map($data);

            $detail_order = json_decode($post['data_paket']);
            foreach ($detail_order as $value) {    
            $this->map_member->tambah_map_detail([
                'id_map_member'  =>  $post['id_map_member'],
                'id_map_ins'    =>  $value->id,
                'tgl_map'            =>  $value->tgl_map
            ]);
            }
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            Data berhasil ditambah
            </div>');
            redirect('Map_member');
        }

        $user           = $this->db->get_where('users', ['email' => $this->session->userdata('email')])->row_array();
        $name           = $user['nama'];
        $img            = $user['img'];
        $date_created   = $user['date_created'];
        $kode           = $this->map_member->CreateCode();
        $inst           = $this->map_member->get_inst();
        $users          = $this->map_member->get_users();

        $data = [
            'head'          => 'Tambah Mapping Member',
            'name'          => $name,
            'img'           => $img,
            'date_created'  => $date_created,
            'kode'          => $kode,
            'inst'          => $inst,
            'users'         => $users
        ];

        $this->load->view('templates/head', $data);
        $this->load->view('templates/nav', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('map_member/add', $data);
        $this->load->view('templates/footer');
    }

    public function edit()
    {
        $post = $this->input->post();
        if ($post) {
            $data = array   (   'id'           =>  $post['id_map_member'],
                                'id_users'     =>  $post['id_users'],
                                'status'       =>  $post['status'],
                            );
            $this->map_member->update($data);

            $order_detail = json_decode($post['data_paket']);
            $this->map_member->delete_detail_map($data['id']);
            foreach ($order_detail as $value) {
                $this->map_member->tambah_map_detail([
                    'id_map_member'      =>  $post['id_map_member'],
                    'id_map_ins'         =>  $value->id_map_ins,
                    'tgl_map'            =>  $value->tgl_map
                ]);
            }
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            Data berhasil diubah
            </div>');
            redirect('Map_member');
        }

        $user = $this->db->get_where('users', ['email' => $this->session->userdata('email')])->row_array();
        $name = $user['nama'];
        $img  = $user['img'];
        $date_created   = $user['date_created'];
        $id             = $this->uri->segment(3);
        $inst           = $this->map_member->get_inst();
        $users          = $this->map_member->get_users();
        $map_member     = $this->map_member->get_map_by_id($id);
        $map_member_detail   = $this->map_member->get_map_detail_by_id($id);

        $data = [
            'title'         => 'Order',
            'head'          => 'Edit Map Member',
            'name'          => $name,
            'img'           => $img,
            'date_created'  => $date_created,
            'inst'          => $inst,
            'users'         => $users,
            'map_member_detail'  => $map_member_detail
        ];

        $this->load->view('templates/head', $data);
        $this->load->view('templates/nav', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('map_member/edit', $map_member);
        $this->load->view('templates/footer');
    }

    public function delete()
    {
        $id = $this->uri->segment(3);
        $this->map_member->hapus();
        $this->map_member->hapus_detail();
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            Data berhasil dihapus
            </div>');
        redirect('map_member');
    }
}

/* End of file Map_member.php */
/* Location: ./application/controllers/Map_member.php */