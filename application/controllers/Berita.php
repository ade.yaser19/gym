<?php 
defined('BASEPATH') or exit ('No direct script access allowed');

class Berita extends CI_Controller {

	public function __construct()
    {
        parent::__construct();
        is_logged_in();
        $this->load->model('Berita_model', 'berita');
    }

    public function index()
    {
        $user = $this->db->get_where('users', ['email' => $this->session->userdata('email')])->row_array();
        $name = $user['nama'];
        $img  = $user['img'];
        $date_created = $user['date_created'];
        $data = [
            'head'          => 'Berita',
            'name'          => $name,
            'img'           => $img,
            'date_created'  => $date_created
        ];

        $data['berita'] = $this->db->get('berita')->result_array();
        foreach ($data['berita'] as $r => $v) {
            
            if ($v['tipe_berita'] == 'P') {
                $data['berita'][$r]['tipe_berita'] = 'Publish';
            } else {
                $data['berita'][$r]['tipe_berita'] = 'Tidak Publish';
            }

            if ($v['status_berita'] == 'A') {
                $data['berita'][$r]['status_berita'] = 'Aktif';
            } else {
                $data['berita'][$r]['status_berita'] = 'Tidak Aktif';
            }

        }
        $this->load->view('templates/head', $data);
        $this->load->view('templates/nav', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('berita/index', $data);
        $this->load->view('templates/footer');
    }

    public function add()
    {
        $post = $this->input->post();
        if ($post) {

            $config['upload_path'] = 'uploads/berita';
            $config['allowed_types'] = '*';
            $config['max_filename'] = '255';
            $config['max_size'] = '1024'; //1 MB

            if (isset($_FILES['image']['name'])) {

                if (0 < $_FILES['image']['error']) {
                
                    echo 'Error during file upload' . $_FILES['image']['error'];
                
                } else {
                
                    if (file_exists('uploads/' . $_FILES['image']['name'])) {
                
                        echo 'File already exists : uploads/' . $_FILES['image']['name'];
                
                    } else {
                
                        $this->load->library('upload', $config);
                
                        if (!$this->upload->do_upload('image')) {
                
                            echo $this->upload->display_errors();
                
                        } else {
                
                            $data = [
                                'judul_berita' => $this->input->post('judul_berita'),
                                'tipe_berita' => $this->input->post('tipe_berita'),
                                'status_berita' => $this->input->post('status_berita'),
                                'isi_berita' => $this->input->post('isi_berita'),
                                'created_date' => date('Y-m-d', strtotime($this->input->post('created_date'))),
                                'foto' => $_FILES['image']['name']
                            ];
                            $this->db->insert('berita', $data);
                            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
                                Data berhasil disimpan
                                </div>');
                            redirect('Berita');
                        }
                    }
                }
            }
        }

        $user = $this->db->get_where('users', ['email' => $this->session->userdata('email')])->row_array();
        $name = $user['nama'];
        $img  = $user['img'];
        $date_created = $user['date_created'];
        $data = [
            'title'         => 'Dashboard',
            'head'          => 'Berita',
            'name'          => $name,
            'img'           => $img,
            'date_created'  => $date_created
        ];
        $this->load->view('templates/head', $data);
        $this->load->view('templates/nav', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('berita/add');
        $this->load->view('templates/footer');
    }

    public function edit()
    {

        $post = $this->input->post();

        if ($post) {

            $data = [
                'judul_berita' => $this->input->post('judul_berita'),
                'tipe_berita' => $this->input->post('tipe_berita'),
                'status_berita' => $this->input->post('status_berita'),
                'isi_berita' => $this->input->post('isi_berita'),
                'created_date' => date('Y-m-d', strtotime($this->input->post('created_date'))),
                // 'foto' => $upload_data['uploads']['file_name']
            ];
            $this->berita->update($data);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
                Data berhasil diubah
                </div>');
            redirect('Berita');
        }

        $user = $this->db->get_where('users', ['email' => $this->session->userdata('email')])->row_array();
        $name = $user['nama'];
        $img  = $user['img'];
        $date_created = $user['date_created'];
        $data = [
            'title'         => 'Dashboard',
            'head'          => 'Berita',
            'name'          => $name,
            'img'           => $img,
            'date_created'  => $date_created
        ];

        $get = $this->berita->detail();
        $this->load->view('templates/head', $data);
        $this->load->view('templates/nav', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('berita/edit', $get);
        $this->load->view('templates/footer');
    }

    public function delete()
    {
        $id = $this->uri->segment(3);
        $this->berita->hapus();
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            Data berhasil dihapus
            </div>');
        redirect('Berita');
    }
}

/* End of file Berita.php */
/* Location: ./application/controllers/Berita.php */