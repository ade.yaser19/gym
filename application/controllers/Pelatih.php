<?php
defined('BASEPATH') or exit('No direct script access allowed');
class Pelatih extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        is_logged_in();
        $this->load->model('Pelatih_model', 'pelatih');
    }

    public function index()
    {
        $user = $this->db->get_where('users', ['email' => $this->session->userdata('email')])->row_array();
        $name = $user['nama'];
        $img  = $user['img'];
        $date_created = $user['date_created'];
        $data = [
            'head'          => 'Pelatih',
            'name'          => $name,
            'img'           => $img,
            'date_created'  => $date_created
        ];

        $data['pelatih'] = $this->db->get('pelatih')->result_array();

        $this->load->view('templates/head', $data);
        $this->load->view('templates/nav', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('pelatih/index', $data);
        $this->load->view('templates/footer');
    }

    public function add()
    {
        $post = $this->input->post();
        if ($post) {            
            $pelatih = [
                'nama_pelatih' => $this->input->post('nama_pelatih'),
                'alamat'       => $this->input->post('alamat'),
                'no_telp'      => $this->input->post('no_telp')
            ];
            $this->db->insert('pelatih', $pelatih);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
                Data berhasil disimpan
                </div>');
            redirect('Pelatih');
        }

        $user = $this->db->get_where('users', ['email' => $this->session->userdata('email')])->row_array();
        $name = $user['nama'];
        $img  = $user['img'];
        $date_created = $user['date_created'];
        $data = [
            'title'         => 'Dashboard',
            'head'          => 'Pelatih',
            'name'          => $name,
            'img'           => $img,
            'date_created'  => $date_created
        ];
        $this->load->view('templates/head', $data);
        $this->load->view('templates/nav', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('pelatih/add');
        $this->load->view('templates/footer');
    }

    public function edit()
    {

        $post = $this->input->post();

        if ($post) {
            $data = [
                'nama_pelatih' => $this->input->post('nama_pelatih'),
                'alamat'       => $this->input->post('alamat'),
                'no_telp'      => $this->input->post('no_telp')
            ];
            $this->pelatih->update($data);
            $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
                Data berhasil diubah
                </div>');
            redirect('Pelatih');
        }

        $user = $this->db->get_where('users', ['email' => $this->session->userdata('email')])->row_array();
        $name = $user['nama'];
        $img  = $user['img'];
        $date_created = $user['date_created'];
        $data = [
            'title'         => 'Dashboard',
            'head'          => 'Pelatih',
            'name'          => $name,
            'img'           => $img,
            'date_created'  => $date_created
        ];

        $get = $this->pelatih->detail();
        $this->load->view('templates/head', $data);
        $this->load->view('templates/nav', $data);
        $this->load->view('templates/sidebar', $data);
        $this->load->view('pelatih/edit', $get);
        $this->load->view('templates/footer');
    }

    public function delete()
    {
        $id = $this->uri->segment(3);
        $this->pelatih->hapus();
        $this->session->set_flashdata('message', '<div class="alert alert-success" role="alert">
            Data berhasil dihapus
            </div>');
        redirect('Pelatih');
    }
}
