<?php 
defined('BASEPATH')or exit('No direct script access allowed');

class Berita_model extends CI_Model {

	public function hapus()
    {
        $id = $this->uri->segment(3);
        $this->db->delete('berita', ['id' => $id]);
    }

    public function update($data)
    {
        $id = $this->input->post('id');
        $this->db->update('berita', $data, ['id' => $id]);
    }
    
    public function detail()
    {
        $id = $this->uri->segment(3);
        return $this->db->get_where('berita', ['id' => $id])->row_array();
    }
}

/* End of file Berita_model.php */
/* Location: ./application/models/Berita_model.php */