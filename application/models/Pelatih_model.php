<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Pelatih_model extends CI_Model
{
    public function get()
    {
        $this->db->select('*');
        $this->db->from('pelatih');
        return $this->db->get()->result_array();
    }

    public function detail()
    {
        $id = $this->uri->segment(3);
        return $this->db->get_where('pelatih', ['id' => $id])->row_array();
    }

    public function update($data)
    {
        $id = $this->input->post('id');
        $this->db->update('pelatih', $data, ['id' => $id]);
    }

    public function hapus()
    {
        $id = $this->uri->segment(3);
        $this->db->delete('pelatih', ['id' => $id]);
    }
}
