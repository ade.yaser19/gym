<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Rekap_model extends CI_Model {

	public function get($tanggal_awal ='', $tanggal_akhir ='')
    {
        $this->db->select('kehadiran.*, users.nama, kelas.nama_kelas, pelatih.nama_pelatih');
        $this->db->from('kehadiran');
        $this->db->join('users', 'users.id = kehadiran.id_users','LEFT');
        $this->db->join('map_member', 'map_member.id_users = users.id','LEFT');
        $this->db->join('map_member_detail', 'map_member_detail.tgl_map = kehadiran.tgl_hadir','LEFT');
        $this->db->join('map_instruktur', 'map_instruktur.id = map_member_detail.id_map_ins','LEFT');
        $this->db->join('kelas', 'kelas.id = map_instruktur.id_kelas','LEFT');
        $this->db->join('pelatih', 'pelatih.id = map_instruktur.id_pelatih','LEFT');
        $this->db->where('kehadiran.status','H');
        if($tanggal_awal !='' && $tanggal_akhir !=''){
            $this->db->where("kehadiran.tgl_hadir BETWEEN '$tanggal_awal' AND '$tanggal_akhir'");
        }
        $this->db->order_by('kehadiran.created_date','DESC');
        return $this->db->get()->result_array();
    }

    public function add_kehadiran($param){
        return $this->db->insert('kehadiran',$param);
    }
}

/* End of file Rekap_model.php */
/* Location: ./application/models/Rekap_model.php */