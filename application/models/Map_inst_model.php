<?php

defined('BASEPATH') or ('No direct script access allowed');

class Map_inst_model extends CI_Model {

	public function get_map_inst()
    {
        $this->db->select('map_instruktur.*, kelas.nama_kelas, pelatih.nama_pelatih');
        $this->db->from('map_instruktur');
        $this->db->join('kelas', 'kelas.id = map_instruktur.id_kelas');
        $this->db->join('pelatih', 'pelatih.id = map_instruktur.id_pelatih');
        $this->db->order_by('map_instruktur.created_date', 'DESC');
        return $this->db->get()->result_array();
    }

    public function detail()
    {
    	$id = $this->uri->segment(3);
        return $this->db->get_where('map_instruktur', ['id' => $id])->row_array();
    }

    public function get_kelas()
    {
        $this->db->select('*');
        $this->db->from('kelas');
        return $this->db->get()->result_object();
    }

    public function get_pelatih()
    {
        $this->db->select('*');
        $this->db->from('pelatih');
        return $this->db->get()->result_object();
    }

    public function check_map_instruktur($id_kelas, $id_pelatih)
    {
        $this->db->select('*');
        $this->db->from('map_instruktur');
        $this->db->where('id_kelas',$id_kelas);
        $this->db->where('id_pelatih',$id_pelatih);
        return $this->db->get()->row_array();
    }

    public function tambah($data)
    {
        $this->db->insert('map_instruktur',$data);
        return $this->db->insert_id();
    }

    public function update($data)
    {
        $id = $this->input->post('id');
        $this->db->update('map_instruktur', $data, ['id' => $id]);
    }

    public function hapus()
    {
        $id = $this->uri->segment(3);
        $this->db->delete('map_instruktur', ['id' => $id]);
    }

    public function CreateCode() {
        $this->db->select('RIGHT(id,6) as id', FALSE);
        $this->db->order_by('id','DESC');    
        $this->db->limit(1);    
        $query = $this->db->get('map_instruktur');
            if($query->num_rows() <> 0){      
                 $data = $query->row();
                 $id = intval($data->id) + 1; 
            }
            else{      
                 $id = 1;  
            }
        $batas = str_pad($id, 6, "0", STR_PAD_LEFT);    
        $kodetampil = "MI".$batas;
        return $kodetampil;  
    }
}

/* End of file Map_inst_model.php */
/* Location: ./application/models/Map_inst_model.php */