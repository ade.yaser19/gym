<?php 
defined('BASEPATH')or exit('No direct script access allowed');

class Map_member_model extends CI_Model {

	public function get_map_member($data)
    {
        $this->db->select('	map_member.*,
                            users.nama,
                            users.email
                            ');
        $this->db->from('map_member');
        $this->db->join('users', 'users.id = map_member.id_users','LEFT');
        return $this->db->get()->result_array();
    }

    public function CreateCode() {
        $this->db->select('RIGHT(id,6) as id', FALSE);
        $this->db->order_by('id','DESC');    
        $this->db->limit(1);    
        $query = $this->db->get('map_member');
            if($query->num_rows() <> 0){      
                 $data = $query->row();
                 $id = intval($data->id) + 1; 
            }
            else{      
                 $id = 1;  
            }
        $batas = str_pad($id, 6, "0", STR_PAD_LEFT);    
        $kodetampil = "MM".$batas;
        return $kodetampil;  
    }

    public function get_inst()
    {
        $this->db->select('*');
        $this->db->from('map_instruktur');
        return $this->db->get()->result_object();
    }

    public function get_users()
    {
        $this->db->select('*');
        $this->db->from('users');
        $this->db->where('role_id','2');
        return $this->db->get()->result_object();
    }

    public function get_map_inst_byId($id)
    {
        $this->db->select(' map_instruktur.*,
                            kelas.nama_kelas,
                            pelatih.nama_pelatih
                            ');
        $this->db->join('kelas', 'kelas.id = map_instruktur.id_kelas','LEFT');
        $this->db->join('pelatih', 'pelatih.id = map_instruktur.id_pelatih','LEFT');
        return $this->db->get_where('map_instruktur', ['map_instruktur.id' => $id])->result_object();
    }

    public function get_map_by_id($id)
    {
        $this->db->select('*');
        $this->db->from('map_member');
        $this->db->where('id', $id);
        return $this->db->get()->row_array();
    }

    public function get_map_detail_by_id($id_map_member)
    {
        $this->db->select('map_member_detail.tgl_map, map_member_detail.id_map_ins, kelas.nama_kelas, pelatih.nama_pelatih');
        $this->db->from('map_member_detail');
        $this->db->join('map_instruktur', 'map_instruktur.id = map_member_detail.id_map_ins','LEFT');
        $this->db->join('kelas', 'kelas.id = map_instruktur.id_kelas','LEFT');
        $this->db->join('pelatih', 'pelatih.id = map_instruktur.id_pelatih','LEFT');
        $this->db->where('map_member_detail.id_map_member', $id_map_member);
        return $this->db->get()->result_object();
    }

    public function tambah_map($data)
    {
        $this->db->insert('map_member',$data);
        return $this->db->insert_id();
    }

    public function tambah_map_detail($data)
    {
        $this->db->insert('map_member_detail',$data);
    }

    public function hapus()
    {
        $id = $this->uri->segment(3);
        $this->db->delete('map_member', ['id' => $id]);
    }

        public function hapus_detail()
    {
        $id = $this->uri->segment(3);
        $this->db->delete('map_member_detail', ['id_map_member' => $id]);
    }

    public function update($data)
    {
        $id = $this->input->post('id');
        $this->db->update('map_member', $data, ['id' => $id]);
    }

    public function delete_detail_map($id)
    {
        $this->db->where('id_map_member',$id);
        $this->db->delete('map_member_detail');
    }
}

/* End of file Map_membe_model.php */
/* Location: ./application/models/Map_membe_model.php */