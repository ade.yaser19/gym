<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Kelas_model extends CI_Model
{
    public function get()
    {
        $this->db->select('*');
        $this->db->from('kelas');
        return $this->db->get()->result_array();
    }

    public function detail()
    {
        $id = $this->uri->segment(3);
        return $this->db->get_where('kelas', ['id' => $id])->row_array();
    }

    public function update($data)
    {
        $id = $this->input->post('id');
        $this->db->update('kelas', $data, ['id' => $id]);
    }

    public function hapus()
    {
        $id = $this->uri->segment(3);
        $this->db->delete('kelas', ['id' => $id]);
    }
}
